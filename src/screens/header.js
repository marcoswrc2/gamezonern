import React from 'react';
import { StyleSheet, Text, View, Dimensions, Image, ImageBackground } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import heartLogo from '../../assets/heart_logo.png';
import Background from '../../assets/game_bg.png';

export default function Header({ navigation, title }) {
  function openMenu() {
    navigation.openDrawer();
  }

  return (
    <ImageBackground source={Background} style={styles.header}>
      <MaterialIcons name='menu' size={28} style={styles.icon} onPress={openMenu} />
      <View style={{ flexDirection: 'row' }}>
        <Image source={heartLogo} style={styles.headerImage} />
        <Text style={styles.headertext}>{title}</Text>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  header: {
    width: Dimensions.get('window').width,
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headertext: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333',
    letterSpacing: 1,
  },
  icon: {
    position: 'absolute',
    left: 16,
  },
  headerImage: {
    width: 26,
    height: 26,
    marginHorizontal: 10,
  },
});
