import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import About from '../src/screens/about';
import Header from '../src/screens/header';

const Stack = createStackNavigator();

export default function AboutStack({ navigation }) {
  return (
    <Stack.Navigator
      initialRouteName='About'
      screenOptions={{
        headerTintColor: '#444',
        headerStyle: {
          backgroundColor: '#BCB7B7',
          height: 70,
        },
      }}
    >
      <Stack.Screen
        name='About'
        component={About}
        options={({ navigation }) => {
          return {
            headerTitle: () => <Header navigation={navigation} title='About' />,
            headerTitleAlign: 'center',
          };
        }}
      />
    </Stack.Navigator>
  );
}
