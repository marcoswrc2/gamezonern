import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../src/screens/home';
import Details from '../src/screens/reviewDetails';
import Header from '../src/screens/header';

const Stack = createStackNavigator();

export default function HomeStack({ navigation }) {
  return (
    <Stack.Navigator
      initialRouteName='Home'
      screenOptions={{
        headerTintColor: '#444',
        headerStyle: {
          backgroundColor: '#BCB7B7',
          height: 70,
        },
      }}
    >
      <Stack.Screen
        name='Home'
        component={HomeScreen}
        options={({ navigation }) => {
          return {
            headerTitle: () => <Header navigation={navigation} title='GameZone' />,
            headerTitleAlign: 'center',
          };
        }}
      />
      <Stack.Screen name='Details' component={Details} options={{ title: 'Review Details' }} />
    </Stack.Navigator>
  );
}
