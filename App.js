import React, { useState } from 'react';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
// import HomeStack from './routes/homeStack';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { View, Text } from 'react-native';
import About from './src/screens/about';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeStack from './routes/homeStack';
import AboutStack from './routes/aboutStack';

const getFonts = () => {
  return Font.loadAsync({
    'nunito-regular': require('./assets/fonts/Nunito-Regular.ttf'),
    'nunito-bold': require('./assets/fonts/Nunito-Bold.ttf'),
  });
};

const Drawer = createDrawerNavigator();

export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  if (fontsLoaded) {
    return (
      <NavigationContainer>
        <Drawer.Navigator initialRouteName='Home'>
          <Drawer.Screen name='Home' component={HomeStack} />
          <Drawer.Screen name='About' component={AboutStack} />
        </Drawer.Navigator>
      </NavigationContainer>
    );
  } else {
    return <AppLoading startAsync={getFonts} onFinish={() => setFontsLoaded(true)} />;
  }
}
